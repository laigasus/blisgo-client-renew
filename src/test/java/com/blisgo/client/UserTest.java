package com.blisgo.client;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import com.blisgo.client.domain.entity.User;
import com.blisgo.client.repository.impl.UserRepositoryImpl;

@SpringBootTest
@Transactional
class UserTest {

	@Autowired
	UserRepositoryImpl userRepository;

	@Test
	@DisplayName("회원가입 잘 되는가")
	void insertTest() {
		User user = User.builder().email("test@test.com").nickname("test").pass("test1234").build();
		userRepository.insertUser(user);
	}

	@Test
	@DisplayName("회원가입 했을 때 memPoint 값이 삽입되었는가?")
	void selectTest() {
		User user = User.builder().email("okjaeook98@gmail.com").pass("dongyang").build();
		User result = userRepository.selectUser(user);
		System.out.println("memPoint결과>" + result.getMemPoint());

		assertThat(result.getMemPoint()).isNotNull();
	}
}
