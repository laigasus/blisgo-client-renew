package com.blisgo.client.web;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.ReplyDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.service.BoardService;
import com.blisgo.client.service.ReplyService;
import com.blisgo.client.util.CloudinaryUtil;

@RestController
@RequestMapping("board")
public class BoardController {

	@Autowired
	private BoardService boardService;

	@Autowired
	private ReplyService replyService;

	CloudinaryUtil cloudinaryUtil;
	String url;

	// 커뮤니티 게시판
	@GetMapping
	public ModelAndView community(ModelAndView mv) {
		ArrayList<BoardDTO> boards = boardService.findBoards();
		mv.addObject("boards", boards);
		url = RouteUrlHelper.combine(folder.community, page.board);
		mv.setViewName(url);
		return mv;
	}

	// 게시판 글내용
	@GetMapping("{bdNo}")
	public ModelAndView content(ModelAndView mv, HttpSession session, BoardDTO boardDTO) {
		boardDTO = boardService.findBoard(boardDTO);
		ArrayList<ReplyDTO> replys = replyService.findReply(boardDTO);
		boardService.countBoardViews(boardDTO);

		mv.addObject("board", boardDTO);
		mv.addObject("replys", replys);
		url = RouteUrlHelper.combine(folder.community, page.content);
		mv.setViewName(url);
		return mv;
	}

	// 게시판 글삭제
	@DeleteMapping("{bdNo}")
	public ModelAndView contentDelete(ModelAndView mv, @Valid BoardDTO boardDTO) {
		boardService.removeBoard(boardDTO);

		url = RouteUrlHelper.combine(page.board);
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 게시판 글 수정 화면
	@GetMapping("edit/{bdNo}")
	public ModelAndView contentEdit(ModelAndView mv, BoardDTO boardDTO) {
		boardDTO = boardService.findBoard(boardDTO);

		mv.addObject("board", boardDTO);
		url = RouteUrlHelper.combine(folder.community, page.edit);
		mv.setViewName(url);
		return mv;
	}

	// 게시판 글 수정 제어
	@PutMapping("edit/{bdNo}")
	public ModelAndView contentEditPut(ModelAndView mv, @Valid BoardDTO boardDTO) {
		boardService.modifyBoard(boardDTO);
		url = RouteUrlHelper.combine(page.board, boardDTO.getBdNo());
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 게시글 좋아요 버튼 눌렀을때
	@PutMapping("{bdNo}/like")
	public ModelAndView likeBoard(ModelAndView mv, HttpSession session, BoardDTO boardDTO, UserDTO userDTO) {
		boardService.countBoardFavorite(boardDTO); // 좋아요 1증가

		url = RouteUrlHelper.combine(page.board, boardDTO.getBdNo());
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 게시판 글작성
	@GetMapping("write")
	public ModelAndView write(ModelAndView mv, HttpSession session) {
		url = RouteUrlHelper.combine(folder.community, page.write);
		mv.setViewName(url);
		return mv;
	}

	// 게시글 이미지 업로드
	@PostMapping("write/upload")
	public @ResponseBody HashMap<String, String> uploadToCloudinary(MultipartFile file) {
		cloudinaryUtil = new CloudinaryUtil();
		String url = cloudinaryUtil.uploadFile(file);
		HashMap<String, String> m = new HashMap<>();
		m.put("link", url);
		return m;
	}

	// 게시판 글 올리기
	@PostMapping("write")
	public ModelAndView writePOST(ModelAndView mv, HttpSession session, @Valid BoardDTO boardDTO, UserDTO userDTO) {
		userDTO = (UserDTO) session.getAttribute("mem");
		boardService.addBoard(boardDTO, userDTO);
		url = RouteUrlHelper.combine(page.board);
		mv.setView(new RedirectView(url, false));
		return mv;
	}

}
