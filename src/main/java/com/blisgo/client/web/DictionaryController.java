package com.blisgo.client.web;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.dto.HashtagDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.service.DictionaryService;

@Controller
@RequestMapping("dictionary")
public class DictionaryController {

	@Autowired
	private DictionaryService dictionaryService;
	String url;

	// 분리배출 사전
	@GetMapping
	public ModelAndView dictionaries(ModelAndView mv, DictionaryDTO dictionaryDTO) {
		ArrayList<DictionaryDTO> dictionaries = dictionaryService.findDictionaries(dictionaryDTO);
		mv.addObject("dictionaries", dictionaries);
		url = RouteUrlHelper.combine(folder.dictionary, page.wastes);
		mv.setViewName(url);
		return mv;
	}

	@PostMapping("more")
	public @ResponseBody ArrayList<DictionaryDTO> dictionaryLoadMore(DictionaryDTO dictionaryDTO) {

		ArrayList<DictionaryDTO> dictionaries = dictionaryService.findDictionaryMore(dictionaryDTO);

		return dictionaries;
	}

	// 분리배출 물품 안내
	@GetMapping("{dicNo}")
	public ModelAndView product(ModelAndView mv, HttpSession session, DictionaryDTO dictionaryDTO) {

		dictionaryDTO = dictionaryService.findDictionary(dictionaryDTO);
		// 조회수(hit) 증가, 이는 곧 전체 제품의 별점 순위를 매기는데 사용됨
		dictionaryService.countDictionaryHit(dictionaryDTO);

		mv.addObject("dictionary", dictionaryDTO);

		ArrayList<HashtagDTO> hashtagAndGuide = dictionaryService.findHashtag(dictionaryDTO);


		ArrayList<DictionaryDTO> relatedDictionaries = dictionaryService.findRelatedDictionaries(hashtagAndGuide);

		mv.addObject("hashtagAndGuide", hashtagAndGuide);
		mv.addObject("relatedDictionaries", relatedDictionaries);

		url = RouteUrlHelper.combine(folder.dictionary, page.waste);
		mv.setViewName(url);
		return mv;
	}

	// 북마크 추가 기능
	@PutMapping("dogam/{dicNo}")
	public @ResponseBody boolean addBookmark(HttpSession session, DictionaryDTO dictionaryDTO, UserDTO userDTO) {
		userDTO = (UserDTO) session.getAttribute("mem");

		if (dictionaryService.addDogam(dictionaryDTO, userDTO)) {
			return true;
		} else {
			return false;
		}
	}

}
