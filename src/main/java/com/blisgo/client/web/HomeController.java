package com.blisgo.client.web;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.service.HomeService;

@Controller
@RequestMapping("/")
public class HomeController {

	// AlgoliaSearch algoliaSearch = new AlgoliaSearch();

	@Autowired
	private HomeService homeService;
	String url;

	@GetMapping
	public ModelAndView index(ModelAndView mv) {
		ArrayList<DictionaryDTO> recentProducts = homeService.findRecentDictionaries();
		mv.addObject("recentProducts", recentProducts);
		url = RouteUrlHelper.combine(folder.cmmn, page.index);
		mv.setViewName(url);

		return mv;
	}

	@GetMapping("faq")
	public ModelAndView faq(ModelAndView mv) {
		url = RouteUrlHelper.combine(folder.cmmn, page.faq);
		mv.setViewName(url);
		return mv;
	}

	@GetMapping("offline")
	public ModelAndView offline(ModelAndView mv) {
		url = RouteUrlHelper.combine(folder.cmmn, page.offline);
		mv.setViewName(url);
		return mv;
	}

}
