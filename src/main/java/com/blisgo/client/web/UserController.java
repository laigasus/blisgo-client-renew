package com.blisgo.client.web;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.blisgo.client.dto.DogamDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.service.UserService;
import com.blisgo.client.util.CloudinaryUtil;
import com.blisgo.client.util.QRCodeGenerator;
import com.blisgo.client.util.SendGridMail;

@RestController
@RequestMapping("user")
public class UserController {
	@Autowired
	private UserService userService;

	SendGridMail sendGridMail;
	CloudinaryUtil cloudinaryUtil;
	String url;

	// 회원 로그인
	@GetMapping("login")
	public ModelAndView login(ModelAndView mv) {
		url = RouteUrlHelper.combine(folder.user, page.login);
		mv.setViewName(url);
		return mv;
	}

	// 회원 로그인 전송
	@PostMapping("login")
	public ModelAndView login(ModelAndView mv, UserDTO userDTO, HttpSession session) {
		UserDTO registerdUser = userService.findUser(userDTO);

		if (registerdUser == null) {
			// alertMsg = new AlertMsg(res, "없는 회원입니다. 회원가입 후 로그인 진행하시길 바랍니다.",
			// "insertBoarder");
			url = RouteUrlHelper.combine(folder.user, page.register);
		} else {
			if (userDTO.getPass().equals(registerdUser.getPass())) {
				session.setAttribute("mem", registerdUser);
				// alertMsg = new AlertMsg(res, "/");
				url = RouteUrlHelper.combine("");
			} else {
				// alertMsg = new AlertMsg(res, "비밀번호가 틀렸습니다. 다시 확인해주세요", "login");
				url = RouteUrlHelper.combine(folder.user, page.login);
			}
		}
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 회원가입
	@GetMapping("register")
	public ModelAndView register(ModelAndView mv) {
		String termsOfAgreement = userService.findTermsOfAgreement();
		mv.addObject("termsOfAgreement", termsOfAgreement);

		url = RouteUrlHelper.combine(folder.user, page.register);
		mv.setViewName(url);
		return mv;

	}

	// 회원가입 전송
	@PostMapping("register")
	public ModelAndView registerPOST(ModelAndView mv, @Valid UserDTO userDTO) {
		if (userService.addUser(userDTO)) {
			// alertMsg = new AlertMsg(res, "회원가입 성공", "login");
			url = RouteUrlHelper.combine(folder.user, page.login);
			mv.setView(new RedirectView(url, false));
		} else {
			// alertMsg = new AlertMsg(res, "회원가입 실패", "register");
			url = RouteUrlHelper.combine(folder.user, page.register);
			mv.setViewName(url);
		}
		return mv;
	}

	// 이메일 중복 확인
	// FIXME [이메일 중복 확인] 프론트단 ajax 수정 필요
//	@PostMapping("register/check")
//	public @ResponseBody boolean registerCheck(UserDTO userDTO) {
//		if (userService.emailCheck(userDTO) > 0) {
//			return false;
//		} else {
//			return true;
//		}
//	}

	// QR 코드 출력
	@GetMapping("qrlogin")
	public ModelAndView qrlogin(ModelAndView mv, HttpSession session, UserDTO userDTO) {
		UserDTO userInfo = (UserDTO) session.getAttribute("mem");

		String imageAsBase64 = null;
		try {
			imageAsBase64 = QRCodeGenerator.qrCreate(userInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		mv.addObject("imageAsBase64", imageAsBase64);
		url = RouteUrlHelper.combine(folder.user, page.qrlogin);
		mv.setViewName(url);
		return mv;
	}

	// 회원 비밀번호 분실 인증
	@GetMapping("verify")
	public ModelAndView verify(ModelAndView mv) {
		url = RouteUrlHelper.combine(folder.user, page.verify);
		return mv;
	}

	// 회원 비밀번호 분실 인증 이메일, 전화번호 전송
	// [회원 비밀번호 분실 인증 이메일, 전화번호 전송] 테스트 불가. 메일전송 API 교체 혹은
	// 전화번호 api 활성. Trustify 또는 Realemail
//	@PostMapping("verify")
//	public ModelAndView verifyEmailPOST(ModelAndView mv, HttpSession session, UserDTO userDTO) {
//
//		if (userService.emailCheck(userDTO) > 0) {
//			String email = userDTO.email();
//			String nickname = userDTO.nickname();
//
//			try {
//				sendGridMail.sendTemplateEmail(email, nickname);
//			} catch (IOException e) {
//				// 메일 전송 실패
//				e.printStackTrace();
//			}
//			session.setAttribute("verifyEmail", email);
//			// alertMsg = new AlertMsg(res, "메일이 전송되었습니다", "/");
//			mv.setViewName("/");
//		} else {
//			// alertMsg = new AlertMsg(res, "없는 계정입니다", "verify");
//			mv.setViewName("verify");
//		}
//		return mv;
//	}

	@GetMapping("chgpw")
	public ModelAndView pwchange(ModelAndView mv, HttpSession session,
			@RequestParam("confirmEmail") String confirmEmail) {
		String verifyEmail = (String) session.getAttribute("verifyEmail");

		if (verifyEmail.equals(confirmEmail)) {
			url = RouteUrlHelper.combine(folder.user, page.chgpw);
		} else {
			session.invalidate();
		}
		mv.setViewName(url);
		return mv;
	}

	// 비밀번호 변경
	// TODO [비밀번호 변경] 사용 위치 파악 후 조치
//	@PutMapping("changepwd")
//	public ModelAndView pwchangeConfirm(ModelAndView mv, HttpSession session, HttpServletRequest req,
//			HttpServletResponse res, UserDTO userDTO) {
//		User user = modelMapper.map(userDTO, User.class);
//
//		String pass = req.getParameter("pw-new");
//		String newPwConfirm = req.getParameter("pw-confirm");
//
//		if (pass.equals(newPwConfirm)) {
//			userService.updatePassword(user, pass);
//			session.invalidate();
//			// alertMsg = new AlertMsg(res, "비밀번호가 변경되었습니다", "/");
//			mv.setViewName("/");
//		} else {
//			// alertMsg = new AlertMsg(res, "입력한 비밀번호가 일치하지 않습니다", "/changepwd");
//			mv.setViewName("changepwd");
//		}
//
//		return mv;
//	}

	// 마이페이지
	@GetMapping("mypage")
	public ModelAndView mypage(ModelAndView mv, HttpSession session, UserDTO userDTO) {
		userDTO = (UserDTO) session.getAttribute("mem");
		ArrayList<DogamDTO> dogamList = userService.findDogam(userDTO);
		mv.addObject("dogamList", dogamList);
		url = RouteUrlHelper.combine(folder.user, page.mypage);
		mv.setViewName(url);
		return mv;
	}

	// 마이페이지 프로필 업데이트
	// FIXME [마이페이지 프로필 업데이트] 회원 정보 변경 즉시 반영되지 않음
	@PutMapping("mypage/update-profile-img")
	public ModelAndView mypageUpdateProfileImg(ModelAndView mv, HttpSession session,
			@RequestParam("upload-img") MultipartFile profile_img, UserDTO userDTO) {
		userDTO = (UserDTO) session.getAttribute("mem");
		cloudinaryUtil = new CloudinaryUtil();
		String profile_img_url = cloudinaryUtil.uploadImage(profile_img);

		userService.modifyUserProfileImg(userDTO, profile_img_url);
		session.removeAttribute("mem");
		session.setAttribute("mem", userDTO);
		url = RouteUrlHelper.combine(folder.user, page.mypage);
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 마이페이지 닉네임 변경
	@PutMapping("mypage/update-nickname")
	public ModelAndView mypageUpdateNickname(ModelAndView mv, HttpSession session, UserDTO userDTO) {

		if (userService.modifyUserNickname(userDTO)) {
			userDTO = (UserDTO) userService.findUser(userDTO);
			session.removeAttribute("mem");
			session.setAttribute("mem", userDTO);
			// alertMsg = new AlertMsg(res, "회원 정보가 변경되었습니다.", "mypage");
			url = RouteUrlHelper.combine(folder.user, page.mypage);
			mv.setView(new RedirectView(url, false));
		} else {
			// alertMsg = new AlertMsg(res, "회원 정보 변경이 실패했습니다.", "/changepwd");
			url = RouteUrlHelper.combine(folder.user, page.chgpw);
			mv.setViewName(url);
		}

		return mv;
	}

	// 회원 비밀번호 변경
	@PutMapping("mypage/update-password")
	public ModelAndView mypageUpdatePassword(ModelAndView mv, HttpSession session, UserDTO userDTO,
			@RequestParam("newPass") String newPass) {
		UserDTO userBefore = (UserDTO) session.getAttribute("mem");

		if (userDTO.getPass().equals(userService.findUser(userBefore).getPass())) {
			userService.modifyUserPass(userBefore, newPass);
			session.invalidate();
			// alertMsg = new AlertMsg(res, "변경된 비밀번호로 다시 로그인바랍니다.", "login");
			url = RouteUrlHelper.combine(folder.user, page.login);
			mv.setView(new RedirectView(url, false));
		} else {
			// alertMsg = new AlertMsg(res, "이전 비밀번호가 틀렸습니다. 다시 확인바랍니다.", "mypage");
			url = RouteUrlHelper.combine(folder.user, page.mypage);
			mv.setViewName(url);
		}
		return mv;
	}

	// 마이페이지 계정 삭제
	@DeleteMapping("mypage")
	public ModelAndView mypageDeleteAccount(ModelAndView mv, HttpSession session) {
		UserDTO userInfo = (UserDTO) session.getAttribute("mem");

		if (userService.removeUser(userInfo)) {
			// alertMsg = new AlertMsg(res, "회원 탈퇴되었습니다.", "/logout");
			session.invalidate();
			url = RouteUrlHelper.combine("");
			mv.setView(new RedirectView(url, false));
		} else {
			// alertMsg = new AlertMsg(res, "회원 탈퇴 실패했습니다. 다시 시도해주시기바랍니다.", "mypage");
			url = RouteUrlHelper.combine(folder.user, page.mypage);
			mv.setViewName(url);
		}
		return mv;
	}

	// TODO [비밀번호 중복 확인] 사용 위치 파악 후 조치
//	@PostMapping("newPassCheck")
//	public @ResponseBody String newPassCheck(String newPass, String passCheck) {
//		boolean result = newPass.equals(passCheck);
//		if (result != true) {
//			return "fail"; // 비밀번호가 같지 않음
//
//		} else {
//			return "success"; // 비밀번호 동일
//		}
//	}

	@PostMapping("dogam/more")
	public @ResponseBody ArrayList<DogamDTO> dictionaryLoadMore(HttpSession session, UserDTO userDTO) {
		userDTO = (UserDTO) session.getAttribute("mem");
		ArrayList<DogamDTO> dictionaries = userService.findDogamMore(userDTO);
		return dictionaries;
	}

	@GetMapping("logout")
	public ModelAndView logout(ModelAndView mv, HttpSession session) {
		session.invalidate();
		url = RouteUrlHelper.combine("");
		mv.setView(new RedirectView(url));
		return mv;
	}
}
