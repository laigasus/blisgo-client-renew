package com.blisgo.client.web;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.ReplyDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.service.ReplyService;

@RestController
@RequestMapping("reply")
public class ReplyController {
	@Autowired
	private ReplyService replyService;
	String url;

	// 댓글 작성
	@PostMapping("{bdNo}")
	public ModelAndView replyPOST(ModelAndView mv, UserDTO userDTO, BoardDTO boardDTO, @Valid ReplyDTO replyDTO,
			HttpServletRequest request) {
		replyService.addReply(replyDTO, boardDTO, userDTO);
		url = RouteUrlHelper.combine(page.board, boardDTO.getBdNo());
		mv.setView(new RedirectView(url, false));
		return mv;
	}

	// 댓글 삭제
	@DeleteMapping("{bdNo}/{replyNo}")
	public ModelAndView replyRemove(ModelAndView mv, BoardDTO boardDTO, ReplyDTO replyDTO, HttpServletRequest request) {
		replyService.removeReply(replyDTO, boardDTO);
		url = RouteUrlHelper.combine(page.board, boardDTO.getBdNo());
		mv.setView(new RedirectView(url, false));
		return mv;
	}
}
