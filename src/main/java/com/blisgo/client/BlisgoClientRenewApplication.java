package com.blisgo.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlisgoClientRenewApplication {

    public static void main(String[] args) {
        SpringApplication.run(BlisgoClientRenewApplication.class, args);
    }

}
