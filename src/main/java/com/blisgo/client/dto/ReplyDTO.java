package com.blisgo.client.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.domain.entity.User;
import com.blisgo.client.util.TimeManager;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class ReplyDTO {
	private Integer replyNo;
	private Board board;
	private User user;
	@NotNull(message = "댓글 내용을 입력해주세요")
	private String content;
	@Null(message = "Controller단에서 계산되는 값입니다.")
	private LocalDateTime createdDate;
	private String timeDiff;

	@Builder
	public ReplyDTO(Integer replyNo, Board board, User user, String content, LocalDateTime createdDate) {
		this.replyNo = replyNo;
		this.board = board;
		this.user = user;
		this.content = content;
		this.createdDate = createdDate;
		this.timeDiff = TimeManager.calcTimeDiff(createdDate);
	}

}
