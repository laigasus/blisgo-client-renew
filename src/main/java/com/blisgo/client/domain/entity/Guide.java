package com.blisgo.client.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Comment;

import com.blisgo.client.domain.entity.cmmn.Wastes;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
public class Guide {
	@Id
	@Enumerated(EnumType.STRING)
	@Comment("가이드 코드(PK)")
	@Column(nullable = false, length = 10)
	private Wastes guideCode;

	@Comment("폐기물 중분류")
	@Column(nullable = false, length = 50)
	private String guideName;

	@Column(nullable = false, length = 200)
	@Comment("폐기물 처리 안내")
	private String guideContent;

	@Column(nullable = false, length = 100)
	@Comment("폐기물 처리 안내 이미지 url")
	private String imagePath;

	@OneToMany(mappedBy = "guide", orphanRemoval = true)
	private List<Hashtag> hashtag = new ArrayList<Hashtag>();

	@Builder
	public Guide(Wastes guideCode, String guideName, String guideContent, String imagePath) {
		this.guideCode = guideCode;
		this.guideName = guideName;
		this.guideContent = guideContent;
		this.imagePath = imagePath;
	}
}