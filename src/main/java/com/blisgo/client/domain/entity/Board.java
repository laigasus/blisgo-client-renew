package com.blisgo.client.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.blisgo.client.domain.entity.cmmn.BaseTimeEntity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@DynamicInsert
@DynamicUpdate
public class Board extends BaseTimeEntity {
	@Id
	@GeneratedValue
	@Comment("글 번호(PK)")
	@Column(name = "bd_no")
	private Integer bdNo;

	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "mem_no", nullable = false)
	@Comment("회원 번호(FK)")
	private User user;

	@Column(nullable = false, length = 45)
	@Comment("글 제목")
	private String bdTitle;

	@Column(length = 45)
	@Comment("글 분류(enum 일반, 공지)")
	private String bdCategory;

	@Column(columnDefinition = "MEDIUMTEXT", nullable = false)
	@Comment("글 내용")
	private String bdContent;

	@ColumnDefault("0")
	@Comment("글 조회수")
	private Integer bdViews;

	@ColumnDefault("0")
	@Comment("글 좋아요")
	private Integer bdFavorite;

	@ColumnDefault("0")
	@Comment("댓글 수")
	private Integer bdReplyCount;

	@Comment("글 썸네일")
	private String bdThumbnail;

	@OneToMany(mappedBy = "board", orphanRemoval = true)
	private List<Reply> reply = new ArrayList<Reply>();

	@Builder
	public Board(Integer bdNo, User user, String bdTitle, String bdCategory, String bdContent,
			Integer bdViews, Integer bdFavorite, Integer bdReplyCount, String bdThumbnail) {
		this.bdNo = bdNo;
		this.user = user;
		this.bdTitle = bdTitle;
		this.bdCategory = bdCategory;
		this.bdContent = bdContent;
		this.bdViews = bdViews;
		this.bdFavorite = bdFavorite;
		this.bdReplyCount = bdReplyCount;
		this.bdThumbnail = bdThumbnail;
	}

	public static Board createBoard(Integer bdNo, User user, String bdTitle, String bdCategory, String bdContent,
			Integer bdViews, Integer bdFavorite, Integer bdReplyCount, String bdThumbnail) {
		return Board.builder().bdNo(bdNo).user(user).bdTitle(bdTitle).bdCategory(bdCategory).bdContent(bdContent)
				.bdViews(bdViews).bdFavorite(bdFavorite).bdReplyCount(bdReplyCount).bdThumbnail(bdThumbnail).build();
	}

	public void putReply(Reply reply) {
		this.reply.add(reply);
	}
}
