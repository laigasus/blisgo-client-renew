package com.blisgo.client.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Comment;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@IdClass(HashtagPK.class)
public class Hashtag {

	@Id
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "dic_no", nullable = false)
	@Comment("사전 번호(FK)")
	private Dictionary dictionary;

	@Id
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "guide_code", nullable = false)
	@Comment("사전 코드(FK)")
	private Guide guide;

	@Builder
	public Hashtag(Dictionary dictionary, Guide guide) {
		this.dictionary = dictionary;
		this.guide = guide;
	}
}

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
class HashtagPK implements Serializable {
	private static final long serialVersionUID = 1L;

	private Dictionary dictionary;
	private Guide guide;

}