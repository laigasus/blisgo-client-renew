package com.blisgo.client.domain.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Comment;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@Entity
@IdClass(DogamPK.class)
public class Dogam {
	@Id
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "mem_no", nullable = false)
	@Comment("회원 번호(PK, FK)")
	private User user;

	@Id
	@ManyToOne
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinColumn(name = "dic_no", nullable = false)
	@Comment("사전 번호(PK, FK)")
	private Dictionary dictionary;

	@Builder
	public Dogam(User user, Dictionary dictionary) {
		this.user = user;
		this.dictionary = dictionary;
	}
}

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
class DogamPK implements Serializable {
	private static final long serialVersionUID = 1L;

	private User user;
	private Dictionary dictionary;
}