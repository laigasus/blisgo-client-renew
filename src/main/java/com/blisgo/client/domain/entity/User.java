package com.blisgo.client.domain.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Comment;
import org.hibernate.annotations.DynamicInsert;

import com.blisgo.client.domain.entity.cmmn.BaseTimeEntity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@DynamicInsert
@Entity
public class User extends BaseTimeEntity {
	@Id
	@GeneratedValue
	@Comment("회원 번호(PK)")
	private Integer memNo;

	@Column(nullable = false, length = 45)
	@Comment("회원 닉네임")
	private String nickname;

	@Column(nullable = false, length = 45)
	@Comment("회원 이메일")
	private String email;

	@Column(nullable = false, length = 200)
	@Comment("회원 비밀번호")
	private String pass;

	@ColumnDefault("0")
	@Comment("회원 포인트")
	private Integer memPoint;

	@Column(nullable = true, length = 100)
	@Comment("회원 프로필 이미지")
	private String profileImage;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private List<Reply> reply = new ArrayList<Reply>();

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private List<Board> board = new ArrayList<Board>();
	
	@OneToMany(mappedBy = "user", orphanRemoval = true)
	private List<Dogam> dogam = new ArrayList<Dogam>();

	@Builder
	public User(Integer memNo, String nickname, String email, String pass, Integer memPoint, String profileImage) {
		this.memNo = memNo;
		this.nickname = nickname;
		this.email = email;
		this.pass = pass;
		this.memPoint = memPoint;
		this.profileImage = profileImage;
	}

	public static User createUser(Integer memNo, String nickname, String email, String pass, Integer memPoint,
			String dogamList, String profileImage) {
		return User.builder().memNo(memNo).nickname(nickname).email(email).pass(pass).memPoint(memPoint)
				.profileImage(profileImage).build();
	}

	public void putReply(Reply reply) {
		this.reply.add(reply);
	}
}
