package com.blisgo.client.domain.entity.cmmn;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.annotations.Comment;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import lombok.Getter;

@Getter
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class BaseTimeEntity {

	@Column(updatable = false, nullable = false)
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Comment("생성시간(공통)")
	private LocalDateTime createdDate;

	@Column(nullable = false)
	@DateTimeFormat(iso = ISO.DATE_TIME)
	@Comment("수정시간(공통)")
	private LocalDateTime modifiedDate;

	@PrePersist
	public void prePersist() {
		this.createdDate = LocalDateTime.now();
		this.modifiedDate = LocalDateTime.now();
	}

	@PreUpdate
	public void preUpdate() {
		this.modifiedDate = LocalDateTime.now();
	}
}
