package com.blisgo.client.service;

import java.util.ArrayList;

import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.UserDTO;

public interface BoardService {

	// 글 등록 메서드(String email, String writer, String category, String title, String
	// content)
	public void addBoard(BoardDTO boardDTO, UserDTO userDTO);

	// 글 목록을 가지고 오는 메서드(페이징 없을때)
	public ArrayList<BoardDTO> findBoards();

	// 글 상세보기 요청을 처리할 메서드(int bdNo)
	public BoardDTO findBoard(BoardDTO boardDTO);

	// 글 삭제 요청을 처리할 메서드(int bdNo)
	public void removeBoard(BoardDTO boardDTO);

	// DB에 있는 view의 값을 증가 시켜주는 메서드(int bdNo, int bdViews)
	public void countBoardViews(BoardDTO boardDTO);

	// 글 수정 메서드(String bdTitle, String bdContent, int bdNo)
	public void modifyBoard(BoardDTO boardDTO);

	// 좋아요 +1(int bdNo, int bdFavorite)
	public void countBoardFavorite(BoardDTO boardDTO);
}
