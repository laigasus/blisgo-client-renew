package com.blisgo.client.service;

import java.util.ArrayList;

import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.dto.HashtagDTO;
import com.blisgo.client.dto.UserDTO;

public interface DictionaryService {

	// 사전 조회 메서드(DictionaryEntity search_dic)
	public ArrayList<DictionaryDTO> findDictionaries(DictionaryDTO dictionaryDTO);

	// 물품 상세 내용 보는 메서드(int dicNo)
	public DictionaryDTO findDictionary(DictionaryDTO dictionaryDTO);

	// 분리배출 가이드 출력 메서드(String getCategoryMid)
	public ArrayList<HashtagDTO> findHashtag(DictionaryDTO dictionaryDTO);

	// 연관 물품 나열 메서드(ArrayList<String> getCategoryMid)
	public ArrayList<DictionaryDTO> findRelatedDictionaries(ArrayList<HashtagDTO> hashtagDTO);

	// 물품 더보기 메서드(DictionaryEntity search_dic)
	public ArrayList<DictionaryDTO> findDictionaryMore(DictionaryDTO dictionaryDTO);

	// 별점 매기는 메서드(int dicNo)
	public void modifyDictionaryPopularity();

	// 조회수 추가 메서드(int dicNo)
	public void countDictionaryHit(DictionaryDTO dictionary);

	// 북마크 기능 메서드(User user, String dicNo)
	public boolean addDogam(DictionaryDTO dictionaryDTO, UserDTO userDTO);
}
