package com.blisgo.client.service;

import java.util.ArrayList;

import com.blisgo.client.dto.DogamDTO;
import com.blisgo.client.dto.UserDTO;

public interface UserService {

	// 회원가입 회원 추가 메서드(User user)
	public boolean addUser(UserDTO userDTO);

	// 회원 전체 정보 조회 메서드(User user)
	public UserDTO findUser(UserDTO userDTO);

	// 이메일 중복 확인 메서드(String email)
	// public int emailCheck(UserDTO userDTO);

	// 회원 닉네임 변경 메서드
	public boolean modifyUserNickname(UserDTO userDTO);

	// 회원 탈퇴 메서드
	public boolean removeUser(UserDTO userDTO);

	// 회원 비밀번호 변경 메서드(String pass, String email)
	public boolean modifyUserPass(UserDTO userDTO, String newPass);

	// 도감 목록 조회 메서드(String dogamList)
	public ArrayList<DogamDTO> findDogam(UserDTO userDTO);

	// 도감 더보기 메서드(String dogamList)
	public ArrayList<DogamDTO> findDogamMore(UserDTO userDTO);

	// 회원 프로필 이미지 변경 메서드(String img_url, String email)
	public void modifyUserProfileImg(UserDTO userDTO, String profile_img_url);

	// 회원가입시 약관 동의서 불러오는 메서드
	public String findTermsOfAgreement();
}
