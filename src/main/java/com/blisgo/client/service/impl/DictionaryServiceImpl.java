package com.blisgo.client.service.impl;

import java.util.ArrayList;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.domain.entity.cmmn.Wastes;
import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.dto.HashtagDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.mapper.DictionaryMapper;
import com.blisgo.client.mapper.HashtagMapper;
import com.blisgo.client.mapper.UserMapper;
import com.blisgo.client.repository.impl.DictionaryRepositoryImpl;
import com.blisgo.client.service.DictionaryService;

@Service
public class DictionaryServiceImpl implements DictionaryService {
	@Autowired
	private DictionaryRepositoryImpl dictionaryRepository;

	private static int index = 0, limit = 24;

	@Override
	public ArrayList<DictionaryDTO> findDictionaries(DictionaryDTO dictionaryDTO) {
		index = 0;
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);
		var rs = dictionaryRepository.selectDictionaryList(dictionary, index, limit);
		return DictionaryMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public DictionaryDTO findDictionary(DictionaryDTO dictionaryDTO) {
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);
		var rs = dictionaryRepository.productInfo(dictionary);
		return DictionaryMapper.INSTANCE.toDTO(rs);
	}

	@Override
	public ArrayList<HashtagDTO> findHashtag(DictionaryDTO dictionaryDTO) {
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);

		var rs = dictionaryRepository.selectHashtagInnerJoinGuide(dictionary);
		return HashtagMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public ArrayList<DictionaryDTO> findRelatedDictionaries(ArrayList<HashtagDTO> hashtagDTO) {
		// 선택한 product의 중분류를 토큰으로 나누어 출력해야할 내용을 저장하여 tabs 출력
		// 선택된 태그중 임의 태그 하나와 관련된 물품들 무작위 4개 조회
		ArrayList<Wastes> tags = new ArrayList<>();

		for (HashtagDTO tag : hashtagDTO) {
			tags.add(tag.getGuide().getGuideCode());
		}

		int size = tags.size();
		Random rd = new Random();
		var rs = dictionaryRepository.selectRelatedDictionaryList(tags.get(rd.nextInt(size)));
		return DictionaryMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public ArrayList<DictionaryDTO> findDictionaryMore(DictionaryDTO dictionaryDTO) {
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);

		// 더보기
		index += limit;

		var rs = dictionaryRepository.selectDictionaryList(dictionary, index, limit);
		return DictionaryMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public void modifyDictionaryPopularity() {
		dictionaryRepository.updateDictionaryPopularity();
	}

	@Override
	public void countDictionaryHit(DictionaryDTO dictionaryDTO) {
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);
		dictionaryRepository.updateDictionaryHit(dictionary);
	}

	@Override
	public boolean addDogam(DictionaryDTO dictionaryDTO, UserDTO userDTO) {
		var dictionary = DictionaryMapper.INSTANCE.toEntity(dictionaryDTO);
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		var dogam = Dogam.builder().dictionary(dictionary).user(user).build();

		if (dictionaryRepository.insertDogam(dogam)) {
			return true;
		} else {
			return false;
		}
	}

}
