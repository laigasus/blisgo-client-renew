package com.blisgo.client.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.mapper.BoardMapper;
import com.blisgo.client.mapper.UserMapper;
import com.blisgo.client.repository.impl.BoardRepositoryImpl;
import com.blisgo.client.service.BoardService;
import com.blisgo.client.util.HtmlContentParse;

@Service
public class BoardServiceImpl implements BoardService {

	@Autowired
	private BoardRepositoryImpl boardRepository;

	@Override
	public void addBoard(BoardDTO boardDTO, UserDTO userDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		var user = UserMapper.INSTANCE.toEntity(userDTO);

		String boardThumbnail = HtmlContentParse.parseThumbnail(board.getBdContent());
		board = Board.builder().bdNo(board.getBdNo()).user(user).bdTitle(board.getBdTitle())
				.bdCategory(board.getBdCategory()).bdContent(board.getBdContent()).bdViews(board.getBdViews())
				.bdFavorite(board.getBdFavorite()).bdReplyCount(board.getBdReplyCount()).bdThumbnail(boardThumbnail)
				.build();
		boardRepository.insertBoard(board);
	}

	@Override
	public ArrayList<BoardDTO> findBoards() {
		String bdContentImgRemoved;
		ArrayList<BoardDTO> board = new ArrayList<>();
		var rs = boardRepository.selectBoardList();
		ArrayList<BoardDTO> boardDTOArray = BoardMapper.INSTANCE.toDTOArrayList(rs);

		for (BoardDTO b : boardDTOArray) {
			bdContentImgRemoved = HtmlContentParse.removeImg(b.getBdContent());
			b = BoardDTO.builder().bdNo(b.getBdNo()).bdTitle(b.getBdTitle()).user(b.getUser())
					.bdCategory(b.getBdCategory()).bdContent(bdContentImgRemoved).bdViews(b.getBdViews())
					.bdFavorite(b.getBdFavorite()).bdReplyCount(b.getBdReplyCount()).bdThumbnail(b.getBdThumbnail())
					.createdDate(b.getCreatedDate()).modifiedDate(b.getModifiedDate()).build();

			board.add(b);
		}

		return board;
	}

	@Override
	public BoardDTO findBoard(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		var rs = boardRepository.selectBoard(board);
		return BoardMapper.INSTANCE.toDTO(rs);
	}

	@Override
	public void removeBoard(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		boardRepository.deleteBoard(board);
	}

	@Override
	public void countBoardViews(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		boardRepository.updateBoardViews(board);
	}

	@Override
	public void modifyBoard(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		boardRepository.updateBoard(board);
	}

	@Override
	public void countBoardFavorite(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		boardRepository.updateBoardFavorite(board);
	}

}
