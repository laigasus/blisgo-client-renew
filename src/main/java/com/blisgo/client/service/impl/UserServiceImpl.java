package com.blisgo.client.service.impl;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blisgo.client.dto.DogamDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.mapper.DogamMapper;
import com.blisgo.client.mapper.UserMapper;
import com.blisgo.client.repository.impl.UserRepositoryImpl;
import com.blisgo.client.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepositoryImpl userRepository;

	private static int index = 0, limit = 24;

	@Override
	public boolean addUser(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		String default_profile_img = "https://ui-avatars.com/api/?background=random&name=" + user.getEmail();
		userRepository.insertUser(user);
		userRepository.updateProfileImg(user, default_profile_img);
		return true;
	}

	@Override
	public UserDTO findUser(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		var rs = userRepository.selectUser(user);
		return UserMapper.INSTANCE.toDTO(rs);

	}

//	@Override
//	public int emailCheck(UserDTO userDTO) {
//		var user = UserMapper.INSTANCE.toEntity(userDTO);
//		var rs = userRepository.emailCheck(user);
//		return rs;
//	}

	@Override
	public boolean modifyUserNickname(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		if (userRepository.updateNickname(user) > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean removeUser(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		userRepository.deleteAccount(user);
		return true;
	}

	@Override
	public boolean modifyUserPass(UserDTO userDTO, String newPass) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		long result = userRepository.updatePassword(user, newPass);
		if (result > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public ArrayList<DogamDTO> findDogam(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		var rs = userRepository.getDogam(user, 0, limit);
		return DogamMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public ArrayList<DogamDTO> findDogamMore(UserDTO userDTO) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		index += limit;

		// 더이상 조회되는 내용이 없을때의 오류 방지
		var rs = userRepository.getDogam(user, index, limit);
		return DogamMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public void modifyUserProfileImg(UserDTO userDTO, String profile_img_url) {
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		userRepository.updateProfileImg(user, profile_img_url);
	}

	@Override
	public String findTermsOfAgreement() {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream in = classLoader.getResourceAsStream("static/agreement.txt");

		InputStreamReader inputStreamReader = new InputStreamReader(in);
		Stream<String> streamOfString = new BufferedReader(inputStreamReader).lines();
		return streamOfString.collect(Collectors.joining());
	}

}
