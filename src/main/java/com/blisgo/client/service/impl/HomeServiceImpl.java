package com.blisgo.client.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.mapper.DictionaryMapper;
import com.blisgo.client.repository.impl.DictionaryRepositoryImpl;
import com.blisgo.client.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {

	@Autowired
	private DictionaryRepositoryImpl dictionaryRepository;

	@Override
	public ArrayList<DictionaryDTO> findRecentDictionaries() {
		var rs = dictionaryRepository.selectRecentDictionaryList();
		return DictionaryMapper.INSTANCE.toDTOArrayList(rs);
	}

}
