package com.blisgo.client.service.impl;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.blisgo.client.domain.entity.Reply;
import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.ReplyDTO;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.mapper.BoardMapper;
import com.blisgo.client.mapper.ReplyMapper;
import com.blisgo.client.mapper.UserMapper;
import com.blisgo.client.repository.impl.ReplyRepositoryImpl;
import com.blisgo.client.service.ReplyService;

@Service
public class ReplyServiceImpl implements ReplyService {
	@Autowired
	private ReplyRepositoryImpl replyRepository;

	@Override
	public ArrayList<ReplyDTO> findReply(BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		var rs = replyRepository.selectReplyInnerJoinUser(board);
		return ReplyMapper.INSTANCE.toDTOArrayList(rs);
	}

	@Override
	public void addReply(ReplyDTO replyDTO, BoardDTO boardDTO, UserDTO userDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		var user = UserMapper.INSTANCE.toEntity(userDTO);
		var reply = ReplyMapper.INSTANCE.toEntity(replyDTO);

		reply = Reply.createReply(reply.getReplyNo(), board, user, reply.getContent());

		replyRepository.insertReply(reply);
		replyRepository.updateReplyCount(board, true);
	}

	@Override
	public void removeReply(ReplyDTO replyDTO, BoardDTO boardDTO) {
		var board = BoardMapper.INSTANCE.toEntity(boardDTO);
		var reply = ReplyMapper.INSTANCE.toEntity(replyDTO);
		replyRepository.deleteReply(reply);
		replyRepository.updateReplyCount(board, false);
	}
}
