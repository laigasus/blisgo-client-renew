package com.blisgo.client.service;

import java.util.ArrayList;

import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.dto.ReplyDTO;
import com.blisgo.client.dto.UserDTO;

public interface ReplyService {
	// 댓글 불러오는 메서드(int bdNo)
	public ArrayList<ReplyDTO> findReply(BoardDTO boardDTO);

	// 댓글 추가하는 메서드(int bdNo, int memNo, String content)
	public void addReply(ReplyDTO replyDTO, BoardDTO boardDTO, UserDTO userDTO);

	// 댓글 삭제 메서드(int replyNo, int bdNo)
	public void removeReply(ReplyDTO replyDTO, BoardDTO boardDTO);
}
