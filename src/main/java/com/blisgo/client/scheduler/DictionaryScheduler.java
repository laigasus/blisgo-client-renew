package com.blisgo.client.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.blisgo.client.service.DictionaryService;

@Component
public class DictionaryScheduler {
	@Autowired
	private DictionaryService dictionaryService;

	@Scheduled(cron = "0 0 */1 * * *")
	public void updateDictionaryPopularity() {
		dictionaryService.modifyDictionaryPopularity();
	}
}
