package com.blisgo.client.mapper.cmmn;

import java.util.ArrayList;

public abstract interface GenericMapper<DTO, Entity> {

	Entity toEntity(DTO dto);

	DTO toDTO(Entity entity);

	ArrayList<DTO> toDTOArrayList(ArrayList<Entity> entityList);

	ArrayList<Entity> toEntityArrayList(ArrayList<DTO> dtoList);
}
