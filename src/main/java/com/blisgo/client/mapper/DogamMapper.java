package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.dto.DogamDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface DogamMapper extends GenericMapper<DogamDTO, Dogam> {
	DogamMapper INSTANCE = Mappers.getMapper(DogamMapper.class);
}
