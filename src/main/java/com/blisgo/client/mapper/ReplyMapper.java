package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Reply;
import com.blisgo.client.dto.ReplyDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface ReplyMapper extends GenericMapper<ReplyDTO, Reply> {
	ReplyMapper INSTANCE = Mappers.getMapper(ReplyMapper.class);
}
