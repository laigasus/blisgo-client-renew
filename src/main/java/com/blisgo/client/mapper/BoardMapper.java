package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.dto.BoardDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface BoardMapper extends GenericMapper<BoardDTO, Board>{
	BoardMapper INSTANCE = Mappers.getMapper(BoardMapper.class);
	
}
