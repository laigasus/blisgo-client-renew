package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.User;
import com.blisgo.client.dto.UserDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface UserMapper extends GenericMapper<UserDTO, User> {
	UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);
}
