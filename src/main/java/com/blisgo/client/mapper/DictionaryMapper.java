package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Dictionary;
import com.blisgo.client.dto.DictionaryDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface DictionaryMapper extends GenericMapper<DictionaryDTO, Dictionary> {
	DictionaryMapper INSTANCE = Mappers.getMapper(DictionaryMapper.class);

}
