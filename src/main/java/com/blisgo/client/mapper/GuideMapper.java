package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Guide;
import com.blisgo.client.dto.GuideDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;


@Mapper
public interface GuideMapper extends GenericMapper<GuideDTO, Guide> {
	GuideMapper INSTANCE = Mappers.getMapper(GuideMapper.class);

}
