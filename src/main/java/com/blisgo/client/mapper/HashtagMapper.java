package com.blisgo.client.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.blisgo.client.domain.entity.Hashtag;
import com.blisgo.client.dto.HashtagDTO;
import com.blisgo.client.mapper.cmmn.GenericMapper;

@Mapper
public interface HashtagMapper extends GenericMapper<HashtagDTO, Hashtag> {
	HashtagMapper INSTANCE = Mappers.getMapper(HashtagMapper.class);
}
