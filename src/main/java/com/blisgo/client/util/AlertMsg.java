package com.blisgo.client.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

public class AlertMsg {
	PrintWriter out;

	public AlertMsg() {

	}

	public AlertMsg(HttpServletResponse res, String msg, String location){
		try {
			out = res.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.println("<script>");
		out.println("alert('" + msg + "');");
		out.println("location.href='" + location + "';");
		out.println("</script>");
		out.flush();
		out.close();
	}

	public AlertMsg(HttpServletResponse res, String location){
		try {
			out = res.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		out.println("<script>");
		out.println("location.href='" + location + "';");
		out.println("</script>");
		out.flush();
		out.close();
	}
}
