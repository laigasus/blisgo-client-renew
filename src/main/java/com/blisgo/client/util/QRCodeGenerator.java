package com.blisgo.client.util;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.blisgo.client.dto.UserDTO;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QRCodeGenerator {
	
	public static String qrCreate(UserDTO userDTO) {
		String string = userDTO.getEmail() + "&" + userDTO.getPass();
		String imageAsBase64 = null;
		try {
			imageAsBase64 = QRCodeGenerator.saveStr(string);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return imageAsBase64;
	}

	public static String saveStr(String string) throws Exception {
		// qr코드 변환 문자열
		String userString = new String(string.getBytes("UTF-8"), "ISO-8859-1");
		// qr코드 바코드 생성값
		int qrcodeColor = 0xFF2e4e96;
		// qr코드 배경색상값
		int backgroundColor = 0xFFFFFFFF;

		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		// 3,4번째 parameter값 : width/height값 지정
		BitMatrix bitMatrix = qrCodeWriter.encode(userString, BarcodeFormat.QR_CODE, 200, 200);
		MatrixToImageConfig matrixToImageConfig = new MatrixToImageConfig(qrcodeColor, backgroundColor);
		BufferedImage bufferedImage = MatrixToImageWriter.toBufferedImage(bitMatrix, matrixToImageConfig);
		// ImageIO를 사용한 QR 저장
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		ImageIO.write(bufferedImage, "png", output);
		return Base64.getEncoder().encodeToString(output.toByteArray());
	}
}
