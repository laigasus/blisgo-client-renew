package com.blisgo.client.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlContentParse {
	public static String parseThumbnail(String str) {
		List<String> list = null;
		list = getImgSrc(str);

		StringBuilder sb = new StringBuilder();
		try {
			sb.append(list.get(0));
			// 49+1
			int appendIndex = sb.indexOf("/v") + 1;

			sb.insert(appendIndex, "c_fill,f_webp,h_400,w_400/");
		} catch (Exception e) {
		}
		return sb.toString();
	}

	public static List<String> getImgSrc(String str) {
		Pattern nonValidPattern = Pattern.compile("<img[^>]*src=[\"']?([^>\"']+)[\"']?[^>]*>");
		List<String> result = new ArrayList<String>();
		Matcher matcher = nonValidPattern.matcher(str);
		while (matcher.find()) {
			result.add(matcher.group(1));
		}
		return result;
	}

	public static String removeImg(String str) {
		Matcher mat;
		Pattern img = Pattern.compile("<img[^>]*>", Pattern.DOTALL);
		mat = img.matcher(str);
		str = mat.replaceAll("");

		Pattern ptagOpen = Pattern.compile("<p>", Pattern.DOTALL);
		mat = ptagOpen.matcher(str);
		str = mat.replaceAll("");

		Pattern ptagClose = Pattern.compile("</p>", Pattern.DOTALL);
		mat = ptagClose.matcher(str);
		str = mat.replaceAll("");

		Pattern brtag = Pattern.compile("<br>", Pattern.DOTALL);
		mat = brtag.matcher(str);
		str = mat.replaceAll(" ");

		return str;
	}
}
