package com.blisgo.client.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.utils.ObjectUtils;

@SuppressWarnings("rawtypes")
public class CloudinaryUtil {
	Cloudinary cloudinary;

	public CloudinaryUtil() {
		// CDN 연결
		cloudinary = new Cloudinary(ObjectUtils.asMap("cloud_name", "hos3yyb97", "api_key", "275985644681352",
				"api_secret", "AGGX9la_4o2pUT0Hr1nwVLjG_oY"));
	}

	public String uploadImage(MultipartFile profile_img) {
		// 최적의 프로필 이미지를 위해 이미지 편집 후 업로드
		Map result = null;
		File uuidFile = null;
		try {
			uuidFile = convert(profile_img);
			result = cloudinary.uploader().upload(convert(profile_img),
					ObjectUtils.asMap("folder", "userprofile", "transformation",
							new Transformation().gravity("auto:classic").width(1000).height(1000).crop("thumb").fetchFormat("webp")));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (uuidFile.exists()) {
				uuidFile.delete();
			}
		}
		return (String) result.get("secure_url");
	}

	public String uploadFile(MultipartFile file) {
		Map result = null;
		File uuidFile = null;
		try {
			uuidFile = convert(file);
			result = cloudinary.uploader().upload(uuidFile, ObjectUtils.asMap("folder", "board"));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (uuidFile.exists()) {
				uuidFile.delete();
			}
		}
		return (String) result.get("secure_url");
	}

	// 파일 이름 변경하여 저장
	public File convert(MultipartFile file) {
		// 파일명 충돌방지
		UUID uuid = UUID.randomUUID();
		String uuidFilename = uuid + "";
		File convFile = new File(uuidFilename);
		try {
			convFile.createNewFile();
			FileOutputStream fos = new FileOutputStream(convFile);
			fos.write(file.getBytes());
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return convFile;
	}
}
