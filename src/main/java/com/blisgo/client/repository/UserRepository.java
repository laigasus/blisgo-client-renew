package com.blisgo.client.repository;

import java.util.ArrayList;

import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.domain.entity.User;

public interface UserRepository {
	public void insertUser(User userEntity);

	public User selectUser(User userEntity);

	//public int emailCheck(User userEntity);

	public long updateNickname(User userEntity);

	public void deleteAccount(User userEntity);

	public long updatePassword(User userEntity, String newPass);

	public ArrayList<Dogam> getDogam(User userEntity, int index, int limit);

	public void updateProfileImg(User userEntity, String profile_img_url);
}
