package com.blisgo.client.repository;

import java.util.ArrayList;

import com.blisgo.client.domain.entity.Dictionary;
import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.domain.entity.Hashtag;
import com.blisgo.client.domain.entity.cmmn.Wastes;

public interface DictionaryRepository {
	public ArrayList<Dictionary> selectRecentDictionaryList();

	public ArrayList<Dictionary> selectDictionaryList(Dictionary dictionaryEntity, int index, int limit);

	public Dictionary productInfo(Dictionary dictionaryEntity);

	public ArrayList<Dictionary> selectRelatedDictionaryList(Wastes guideCode);

	public void updateDictionaryPopularity();

	public void updateDictionaryHit(Dictionary dictionaryEntity);
	
	public ArrayList<Hashtag> selectHashtagInnerJoinGuide(Dictionary dictionaryEntity);
	
	public boolean insertDogam(Dogam dogamEntity);
}
