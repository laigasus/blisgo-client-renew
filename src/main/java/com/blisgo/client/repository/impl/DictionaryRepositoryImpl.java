package com.blisgo.client.repository.impl;

import static com.blisgo.client.domain.entity.QDictionary.dictionary;
import static com.blisgo.client.domain.entity.QGuide.guide;
import static com.blisgo.client.domain.entity.QHashtag.hashtag;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blisgo.client.domain.entity.Dictionary;
import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.domain.entity.Hashtag;
import com.blisgo.client.domain.entity.cmmn.Wastes;
import com.blisgo.client.repository.DictionaryRepository;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional
@Repository
public class DictionaryRepositoryImpl implements DictionaryRepository {

	@Autowired
	private final JPAQueryFactory jpaQueryFactory;

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	private EntityManager entityManager;

	public DictionaryRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		this.jpaQueryFactory = jpaQueryFactory;
	}

	@Override
	public ArrayList<Dictionary> selectRecentDictionaryList() {
		return (ArrayList<Dictionary>) jpaQueryFactory.selectFrom(dictionary).orderBy(dictionary.dicNo.desc()).limit(12)
				.fetch();
	}

	@Override
	public ArrayList<Dictionary> selectDictionaryList(Dictionary dictionaryEntity, int index, int limit) {
		return (ArrayList<Dictionary>) jpaQueryFactory.selectFrom(dictionary).offset(index).limit(limit).fetch();
	}

	@Override
	public Dictionary productInfo(Dictionary dictionaryEntity) {
		return jpaQueryFactory.selectFrom(dictionary).where(dictionary.dicNo.eq(dictionaryEntity.getDicNo()))
				.fetchOne();
	}

	@Override
	public ArrayList<Dictionary> selectRelatedDictionaryList(Wastes guideCode) {
		List<Hashtag> relatedWastes = jpaQueryFactory.selectFrom(hashtag).where(hashtag.guide.guideCode.eq(guideCode))
				.orderBy(Expressions.numberTemplate(Integer.class, "function('rand')").asc()).limit(4).fetch();

		ArrayList<Integer> dicNos = new ArrayList<>();
		for (Hashtag relatedWaste : relatedWastes) {
			dicNos.add(relatedWaste.getDictionary().getDicNo());
		}

		return (ArrayList<Dictionary>) jpaQueryFactory.selectFrom(dictionary).where(dictionary.dicNo.in(dicNos))
				.fetch();
	}

	// FIXME [별점 매기기] QueryDSL 에서 update join on을 지원하지 않음. 다른 방법으로 구현해야 함
	@Modifying
	@Override
	public void updateDictionaryPopularity() {
		String sql = "update dictionary inner join(select *, NTILE(10) OVER (ORDER BY hit ASC) as star from dictionary) dic on dic.dic_no=dictionary.dic_no set dictionary.popularity=dic.star";
		jdbcTemplate.update(sql);
	}

	@Modifying
	@Override
	public void updateDictionaryHit(Dictionary dictionaryEntity) {
		jpaQueryFactory.update(dictionary).set(dictionary.hit, dictionary.hit.add(1))
				.where(dictionary.dicNo.eq(dictionaryEntity.getDicNo())).execute();
	}

	@Override
	public ArrayList<Hashtag> selectHashtagInnerJoinGuide(Dictionary dictionaryEntity) {
		return (ArrayList<Hashtag>) jpaQueryFactory.selectFrom(hashtag).innerJoin(guide)
				.on(hashtag.guide.guideCode.eq(guide.guideCode))
				.where(hashtag.dictionary.dicNo.eq(dictionaryEntity.getDicNo())).fetch();

	}

	@Modifying
	@Override
	public boolean insertDogam(Dogam dogamEntity) {
		try {
			entityManager.merge(dogamEntity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
