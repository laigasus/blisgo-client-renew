package com.blisgo.client.repository.impl;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.blisgo.client.repository.GuideRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class GuideRepositoryImpl implements GuideRepository {
	@Autowired
	@SuppressWarnings("unused")
	private final JPAQueryFactory jpaQueryFactory;

	@Autowired
	@SuppressWarnings("unused")
	private EntityManager entityManager;

	public GuideRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		this.jpaQueryFactory = jpaQueryFactory;
	}

}
