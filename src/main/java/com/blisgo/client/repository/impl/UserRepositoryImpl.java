package com.blisgo.client.repository.impl;

import static com.blisgo.client.domain.entity.QDictionary.dictionary;
import static com.blisgo.client.domain.entity.QDogam.dogam;
import static com.blisgo.client.domain.entity.QUser.user;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blisgo.client.domain.entity.Dogam;
import com.blisgo.client.domain.entity.User;
import com.blisgo.client.repository.UserRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional
@Repository
public class UserRepositoryImpl implements UserRepository {
	@Autowired
	private final JPAQueryFactory jpaQueryFactory;

	@Autowired
	private EntityManager entityManager;

	public UserRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		this.jpaQueryFactory = jpaQueryFactory;
	}

	@Modifying
	@Override
	public void insertUser(User userEntity) {
		entityManager.persist(userEntity);
	}

	@Override
	public User selectUser(User userEntity) {
		return jpaQueryFactory.selectFrom(user).where(user.email.eq(userEntity.getEmail())).fetchOne();
	}

//	@Override
//	public int emailCheck(User userEntity) {
//		return jpaQueryFactory.selectFrom(user).where(user.email.eq(userEntity.getEmail())).fetch().size();
//	}

	@Modifying
	@Override
	public long updateNickname(User userEntity) {
		return jpaQueryFactory.update(user).where(user.email.eq(userEntity.getEmail()))
				.set(user.nickname, userEntity.getNickname()).execute();
	}

	@Modifying
	@Override
	public void deleteAccount(User userEntity) {
		jpaQueryFactory.delete(user).where(user.memNo.eq(userEntity.getMemNo())).execute();

	}

	@Modifying
	@Override
	public long updatePassword(User userEntity, String newPass) {
		return jpaQueryFactory.update(user).set(user.pass, newPass).where(user.email.eq(userEntity.getEmail()))
				.execute();
	}

	@Override
	public ArrayList<Dogam> getDogam(User userEntity, int index, int limit) {

		return (ArrayList<Dogam>) jpaQueryFactory.selectFrom(dogam).innerJoin(dictionary)
				.on(dogam.dictionary.dicNo.eq(dictionary.dicNo)).where(dogam.user.memNo.eq(userEntity.getMemNo()))
				.offset(index).limit(limit).fetch();
	}

	@Modifying
	@Override
	public void updateProfileImg(User userEntity, String profile_img_url) {
		jpaQueryFactory.update(user).set(user.profileImage, profile_img_url).where(user.email.eq(userEntity.getEmail()))
				.execute();
	}

}
