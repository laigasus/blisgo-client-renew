package com.blisgo.client.repository.impl;

import static com.blisgo.client.domain.entity.QBoard.board;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.repository.BoardRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional
@Repository
public class BoardRepositoryImpl implements BoardRepository {

	@Autowired
	private final JPAQueryFactory jpaQueryFactory;

	@Autowired
	@SuppressWarnings("unused")
	private EntityManager entityManager;

	public BoardRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		this.jpaQueryFactory = jpaQueryFactory;
	}

	@Modifying
	@Override
	public void insertBoard(Board boardEntity) {
		entityManager.persist(boardEntity);
	}

	@Override
	public ArrayList<Board> selectBoardList() {
		return (ArrayList<Board>) jpaQueryFactory.selectFrom(board).orderBy(board.bdNo.desc()).fetch();
	}

	@Override
	public Board selectBoard(Board boardEntity) {
		return jpaQueryFactory.selectFrom(board).where(board.bdNo.eq(boardEntity.getBdNo())).fetchOne();
	}

	@Modifying
	@Override
	public void deleteBoard(Board boardEntity) {
		jpaQueryFactory.delete(board).where(board.bdNo.eq(boardEntity.getBdNo())).execute();
	}

	@Modifying
	@Override
	public void updateBoardViews(Board boardEntity) {
		jpaQueryFactory.update(board).set(board.bdViews, board.bdViews.add(1))
		.where(board.bdNo.eq(boardEntity.getBdNo())).execute();
	}

	@Modifying
	@Override
	public void updateBoard(Board boardEntity) {
		jpaQueryFactory.update(board).set(board.bdTitle, boardEntity.getBdTitle())
		.set(board.bdContent, boardEntity.getBdContent()).where(board.bdNo.eq(boardEntity.getBdNo())).execute();
	}

	@Modifying
	@Override
	public void updateBoardFavorite(Board boardEntity) {
		jpaQueryFactory.update(board).set(board.bdFavorite, board.bdFavorite.add(1))
		.where(board.bdNo.eq(boardEntity.getBdNo())).execute();
	}

}
