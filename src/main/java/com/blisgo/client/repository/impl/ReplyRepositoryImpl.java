package com.blisgo.client.repository.impl;

import static com.blisgo.client.domain.entity.QBoard.board;
import static com.blisgo.client.domain.entity.QReply.reply;
import static com.blisgo.client.domain.entity.QUser.user;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.domain.entity.Reply;
import com.blisgo.client.repository.ReplyRepository;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Transactional
@Repository
public class ReplyRepositoryImpl implements ReplyRepository {

	@Autowired
	private final JPAQueryFactory jpaQueryFactory;

	@Autowired
	private EntityManager entityManager;

	public ReplyRepositoryImpl(JPAQueryFactory jpaQueryFactory) {
		this.jpaQueryFactory = jpaQueryFactory;
	}

	@Override
	public ArrayList<Reply> selectReplyInnerJoinUser(Board boardEntity) {
		return (ArrayList<Reply>) jpaQueryFactory.selectFrom(reply).innerJoin(user)
				.on(reply.user.memNo.eq(user.memNo)).where(reply.board.bdNo.eq(boardEntity.getBdNo())).fetch();
	}

	@Modifying
	@Override
	public void updateReplyCount(Board boardEntity, boolean flag) {
		if (flag) {
			jpaQueryFactory.update(board).set(board.bdReplyCount, board.bdReplyCount.add(1))
					.where(board.bdNo.eq(boardEntity.getBdNo())).execute();
		} else {
			jpaQueryFactory.update(board).set(board.bdReplyCount, board.bdReplyCount.subtract(1))
					.where(board.bdNo.eq(boardEntity.getBdNo())).execute();
		}

	}

	@Modifying
	@Override
	public void insertReply(Reply replyEntity) {
		entityManager.persist(replyEntity);

	}

	@Modifying
	@Override
	public void deleteReply(Reply replyEntity) {
		jpaQueryFactory.delete(reply).where(reply.replyNo.eq(replyEntity.getReplyNo())).execute();

	}

}
