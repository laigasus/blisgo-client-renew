package com.blisgo.client.repository;

import java.util.ArrayList;

import com.blisgo.client.domain.entity.Board;

public interface BoardRepository {
	public void insertBoard(Board boardEntity);

	public ArrayList<Board> selectBoardList();

	public Board selectBoard(Board boardEntity);

	public void deleteBoard(Board boardEntity);

	public void updateBoardViews(Board boardEntity);

	public void updateBoard(Board boardEntity);

	public void updateBoardFavorite(Board boardEntity);

}
