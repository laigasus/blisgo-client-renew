package com.blisgo.client.repository;

import java.util.ArrayList;

import com.blisgo.client.domain.entity.Board;
import com.blisgo.client.domain.entity.Reply;

public interface ReplyRepository {
	public ArrayList<Reply> selectReplyInnerJoinUser(Board boardEntity);

	public void updateReplyCount(Board boardEntity, boolean flag);

	public void insertReply(Reply replyEntity);

	public void deleteReply(Reply replyEntity);
}
