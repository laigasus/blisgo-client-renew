$('#loading').css("display", "none");

$(document).ready(function () {
	var flag = true;
	

	$(document).on('scroll', $.throttle(500, function () {
		check_if_needs_more_content();
	}));

	function check_if_needs_more_content() {
		
		var pixelsFromWindowBottomToBottom = 0 + $(document).height() - $(window).scrollTop() - $(window).height();

		//console.log($(document).height());
		//console.log($(window).scrollTop());
		//console.log($(window).height());
		//console.log(pixelsFromWindowBottomToBottom);

		if (pixelsFromWindowBottomToBottom < 300) {
			if (flag) {
                $('#loading').css("display", "inline");
				$.ajax({
					type: "post",
					url: "/dictionary/more",
					cache: false,
					async: true,
					success: function (dictionaries) {
						$.each(dictionaries, function (i, data) {
							var content = document.createElement('div');
							content.className = 'col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3 col-xxl-2 p-1 product';
							content.innerHTML += "<figure class='figure position-relative mb-0' data-aos='zoom-in'>"
								+ "<a href='/dictionary/" + data.dicNo + "'>"
								+ "<img class='img-fluid' src='" + data.thumbnail + "' width='640px' height='640px' alt='사전 이미지'>"
								+ "</a>"
								+ "<figcaption class='figure-caption text-center bg-black bg-opacity-50 p-1 text-white position-absolute bottom-0 translate-middle-x start-50 w-100'>" + data.name + "</figcaption>"
								+ "</figure>";
							$('#dictionaries').append(content);
						});
						if (dictionaries.length == 0) {
							console.log("더이상 없습니다.");
							flag = false;
						}
					},
					error: function () {
						console.log("조회 실패");
						flag = false;
					},
					complete: function () {
						$('#loading').css("display", "none");
					}
				})
			}
		}
	}
});